import atexit
import sys

from fastapi import APIRouter
from fastapi import HTTPException

from db import acquire_lock, release_lock, r
from shemas import CalculatedValueEntity, CreateValuesRequest
from utils import calculate_prime_factors, is_prime

natural_number_router = APIRouter()


@natural_number_router.on_event("startup")
async def startup_event():
    db_index = acquire_lock(r)
    if db_index is None:
        sys.exit("Error: Unable to acquire lock for Redis database.")
    print(f"Connected to Redis database {db_index}")
    atexit.register(lambda: release_lock(r, db_index))


@natural_number_router.post("")
async def create_values(request: CreateValuesRequest, ttl: int = None):
    if not r.ping():
        raise HTTPException(status_code=500, detail="Failed to connect to Redis")

    results = []

    for value in request.root:
        if value <= 1:
            raise HTTPException(status_code=400, detail="Value must be greater than 1")

        prime_factors = calculate_prime_factors(value)
        result = CalculatedValueEntity(value=value, prime_factors=prime_factors)
        results.append(result)

        key = f"values:{value}:prime_factors"
        r.set(key, ",".join(map(str, prime_factors)))
        if ttl:
            r.expire(key, ttl)

    return results


@natural_number_router.get("/{prime_factor}")
async def read_values(prime_factor: int):
    if not is_prime(prime_factor):
        raise HTTPException(
            status_code=400, detail="Prime factor must be a prime number"
        )

    results = []

    for key in r.scan_iter(match="values:*:prime_factors"):
        value = int(key.split(":")[1])
        prime_factors_str = r.get(key)
        prime_factors = list(map(int, prime_factors_str.split(",")))

        if prime_factor in prime_factors:
            result = CalculatedValueEntity(value=value, prime_factors=prime_factors)
            results.append(result)

    return results
