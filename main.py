import uvicorn
from fastapi import FastAPI

from routers import natural_number_router

app = FastAPI()

app.include_router(natural_number_router, prefix="/values")


if __name__ == "__main__":
    uvicorn.run(
        app,
        host="0.0.0.0",
        port=8080,
    )
