import os

import redis

redis_host = os.getenv("REDIS_HOST", "127.0.0.1")
redis_port = int(os.getenv("REDIS_PORT", 6379))
redis_db = int(os.getenv("REDIS_DB", 0))

r = redis.StrictRedis(
    host=redis_host,
    port=redis_port,
    db=redis_db,
    decode_responses=True,
)


NUM_REDIS_DBS = 16
LOCK_KEYS = [f"lock:{i}" for i in range(NUM_REDIS_DBS)]


def acquire_lock(redis_client):
    for lock_key in LOCK_KEYS:
        if redis_client.set(lock_key, "lock", nx=True):
            return int(lock_key.split(":")[1])
    return None


def release_lock(redis_client, db_index):
    redis_client.delete(f"lock:{db_index}")
