from pydantic import BaseModel, RootModel


CreateValuesRequest = RootModel[list[int]]


class CalculatedValueEntity(BaseModel):
    value: int
    prime_factors: list[int]


SetValueResponse = RootModel[list[CalculatedValueEntity]]
